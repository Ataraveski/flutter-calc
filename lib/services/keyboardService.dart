import 'package:calculator/component/keyMeta.dart';
import 'package:calculator/component/calcActions.dart';
import 'package:flutter/material.dart';

import 'calcServiceInterface.dart';

class KeyboardService extends StatelessWidget implements CalcService {
  KeyboardService({Key? key, required TextEditingController listener})
      : _listener = listener,
        super(key: key);

  final TextEditingController _listener;
  final RegExp regex = RegExp(r'([.]*0)(?!.*\d)');

  bool cursor = false;

  double? _currentValue = 0;
  CalcActions _currentOperation = CalcActions.NOT_IMPLEMENTED;
  double Function(double a, double b) _operationFunction =
      (double a, double b) => 0;

  @override
  Widget build(BuildContext context) {
    return const Placeholder();
  }

  @override
  void process(String value, KeyMeta meta) {
    if (meta.type == KeyMetaType.operation || meta.type == KeyMetaType.unary_operation) {
      print('Operation ${meta.label}');
      cursor = true;
      if (_currentOperation != CalcActions.NOT_IMPLEMENTED) {
        _listener.text = calc(double.tryParse(_listener.text));
      } else {
        _currentOperation = meta.action;
      }
      _currentValue = double.tryParse(_listener.text);
    }

    switch (meta.action) {
      case CalcActions.NOT_IMPLEMENTED:
        // TODO: Handle this case.
        print('Not implmented ${meta}');
        break;
      case CalcActions.CLEAR:
        _listener.text = '0';
        break;
      case CalcActions.CLEAR_ALL:
        _currentOperation = CalcActions.NOT_IMPLEMENTED;
        _currentValue = 0;
        _listener.text = '0';
        _operationFunction = (double a, double b) => 0;
        break;
      case CalcActions.BACKSPACE:
        String currentText = _listener.text;
        if (currentText.length > 1) {
          _listener.text = currentText.substring(1, currentText.length - 1);
        } else {
          _listener.text = '0';
        }
        break;
      case CalcActions.PLUS:
        _operationFunction = (double a, double b) => a + b;
        break;
      case CalcActions.MINUS:
        _operationFunction = (double a, double b) => a - b;
        break;
      case CalcActions.DIVISION:
        _operationFunction = (double a, double b) => a / b;
        break;
      case CalcActions.MULTIPLY:
        _operationFunction = (double a, double b) => a * b;
        break;
      case CalcActions.COMMA:
        if (!_listener.text.contains('.')) {
          _listener.text += '.';
        }
        break;
      case CalcActions.CHANGE_SIGN:
        if (_listener.text.startsWith('-')) {
          _listener.text = _listener.text.replaceFirst('-', '');
        } else {
          _listener.text = '-${_listener.text}';
        }
        break;
      case CalcActions.SQRT:
        // TODO: Handle this case.
        break;
      case CalcActions.RESULT:
        // TODO: Handle this case.
        break;
      case CalcActions.PRESS_NUMBER:
        _listener.text = validateInput(_listener.text, meta.label, cursor);
        cursor = false;
        break;
    }
  }

  void digitsProcess(KeyMeta meta) {
    if (_listener.text.startsWith('0')) {
      _listener.text = meta.label;
    } else {
      _listener.text += meta.label;
    }
  }

  String validateInput(String text, String label, bool cursor) {
    print(cursor);
    double? numericInput = double.tryParse(cursor ? label : text + label);
    return numericInput.toString().replaceAll(regex, '');
  }

  String calc(double? secondValue) {
    return _operationFunction(_currentValue!, secondValue!)
        .toString()
        .replaceAll(regex, '');
  }
}
