
import 'package:calculator/services/keyboardService.dart';
import 'package:flutter/material.dart';

import '../elements/commonKeyBoard.dart';
import '../elements/mainTextPanel.dart';
import '../elements/systemButtons.dart';



class MainCalcPage extends StatefulWidget {
  const MainCalcPage({Key? key}) : super(key: key);

  @override
  State<MainCalcPage> createState() => _MainCalcPageState();
}

class _MainCalcPageState extends State<MainCalcPage>  {

  final TextEditingController _historyController = TextEditingController();
  final TextEditingController _inputController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    _historyController.text = "5+3=\n8";
    _inputController.text = "0";

    // changeText(String text){
    //   setState; {
    //     _inputController.text = text;
    //   }
    // };

    return Scaffold(
        backgroundColor: Color(0xFF1c2123),
        // appBar: ,
        // bottomSheet: ,
        body: SafeArea(
            child: Padding(
                padding: const EdgeInsets.all(5),
                child: Column(
                  children: [
                    DefaultTabController(
                      length: 2,
                      child: Column(
                        children: [
                          Container(
                            color: const Color(0xFF23282B),
                            child: TabBar(
                              padding: const EdgeInsets.only(right: 200),
                              indicatorColor: const Color(0xFFE76608),
                              //  mainAxisAlignment: MainAxisAlignment.start,
                              tabs: [
                                Container(
                                  width: 70,
                                  height: 20,
                                  // color: Colors.green,
                                  child: const InkWell(
                                    child: Text(
                                      'Журнал',
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  ),
                                ),
                                const SizedBox(
                                  width: 70,
                                  height: 20,
                                  //color: Colors.amber,
                                  child: InkWell(
                                    child: Text(
                                      'Память',
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            width: 380,
                            height: 200,
                            child: TabBarView(children: [
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Container(
                                    width: 380,
                                    height: 200,
                                    color: const Color(0xFF23282B),
                                    alignment: Alignment.center,
                                    child: TextField(
                                      readOnly: true,
                                      controller: _historyController,
                                      style: const TextStyle(
                                        color: Colors.white,
                                      ),
                                      textAlign: TextAlign.end,
                                      maxLines: null,
                                      expands: true,
                                    ),
                                    //  ),
                                  )
                                ],
                              ),
                              Text('Test'),
                            ]),
                          ),
                        ],
                      ),
                    ),
                    Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              width: 120,
                              height: 30,
                              //  color: Colors.lightBlue,
                              child: InkWell(
                                child: Row(
                                  children: [
                                    Padding(
                                        padding:
                                            EdgeInsets.only(right: 8, left: 5),
                                        child: Image.asset(
                                          'assets/icons/menu.png',
                                          width: 25,
                                          height: 25,
                                        )),
                                    const Text(
                                      'Обычный',
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            // Container(
                            //   width: 30,
                            //   height: 30,
                            //   //   color: Colors.lime,
                            //   child: const InkWell(
                            //     child: Icon(
                            //       Icons.settings,
                            //       color: Colors.white,
                            //       size: 30,
                            //     ),
                            //     // Image.asset(
                            //     //   'assets/icons/history.png',
                            //     //   width: 25,
                            //     //   height: 25,
                            //     // ),
                            //   ),
                            // ),
                          ],
                        ),
                        Row(
                          children: [
                            CommonTextPanel(
                              inputController: _inputController,
                            ),
                          ],
                        ),
                      ],
                    ),
                    SystemKeyboard(),
                    CommonKeyboard(
                      service: KeyboardService(
                        listener: _inputController,
                      ),
                    ),
                    // Column(
                    //   children: keyboard,
                    // ),
                  ],
                ))));
  }
}
