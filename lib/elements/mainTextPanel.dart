import 'package:flutter/material.dart';

class CommonTextPanel extends StatelessWidget {
  const CommonTextPanel(
      {Key? key, required TextEditingController? inputController})
      : _inputController = inputController,
        super(key: key);

  final TextEditingController? _inputController;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 380,
      height: 100,
      //   color: Colors.grey,
      alignment: Alignment.bottomRight,
      child: TextField(
        readOnly: true,
        textAlignVertical: TextAlignVertical.bottom,
        controller: _inputController,
        style: const TextStyle(
          color: Colors.white,
          fontSize: 45,
        ),
        textAlign: TextAlign.end,
        maxLines: 1,
        // expands: true,
      ),
    );
  }
}
