import 'package:calculator/component/calcActions.dart';
import 'package:flutter/material.dart';

import '../component/keyMeta.dart';

class SystemKeyboard extends StatelessWidget {
  SystemKeyboard({Key? key}) : super(key: key);

  final List<KeyMeta> _systemkeys = [
    KeyMeta(KeyMetaType.system, 'MC', CalcActions.NOT_IMPLEMENTED),
    KeyMeta(KeyMetaType.system, 'MR', CalcActions.NOT_IMPLEMENTED),
    KeyMeta(KeyMetaType.system, 'M+', CalcActions.NOT_IMPLEMENTED),
    KeyMeta(KeyMetaType.system, 'M-', CalcActions.NOT_IMPLEMENTED),
    KeyMeta(KeyMetaType.system, 'MS', CalcActions.NOT_IMPLEMENTED),
    KeyMeta(KeyMetaType.system, 'M', CalcActions.NOT_IMPLEMENTED),
  ];



  @override
  Widget build(BuildContext context) {

    List<Widget> systemButtons = [];

    for (var element in _systemkeys) {
      systemButtons.add(Padding(
        padding: const EdgeInsets.only(top: 2, bottom: 2),
        child: InkWell(
          child: Container(
              width: MediaQuery.of(context).size.width / 6 - 2,
              height: 30,
              child: Image.asset('assets/icons/${element.label}.png')),
          onTap: () {
            print(element.label);
          },
        ),
      ));
    }
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: systemButtons,
    );
  }
}
