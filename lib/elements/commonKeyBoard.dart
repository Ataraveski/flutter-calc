import 'package:flutter/material.dart';

import '../component/calcActions.dart';
import '../component/keyMeta.dart';
import '../services/calcServiceInterface.dart';

class CommonKeyboard extends StatelessWidget {
  CommonKeyboard({Key? key, required CalcService service})
      : _service = service,
        super(key: key);

  final CalcService _service;
  final List<List<KeyMeta>> _keys = [
    [
      KeyMeta(KeyMetaType.operation, '%', CalcActions.PERCENT),
      KeyMeta(KeyMetaType.operation, 'CE', CalcActions.CLEAR_ALL),
      KeyMeta(KeyMetaType.operation, 'C', CalcActions.CLEAR),
      KeyMeta(KeyMetaType.operation, 'backspace', CalcActions.BACKSPACE),
    ],
    [
      KeyMeta(KeyMetaType.unary_operation, '1x', CalcActions.FRACTION),
      KeyMeta(KeyMetaType.unary_operation, 'sqrt', CalcActions.SQRT),
      KeyMeta(KeyMetaType.unary_operation, 'root', CalcActions.ROOT),
      KeyMeta(KeyMetaType.operation, 'division', CalcActions.DIVISION),
    ],
    [
      KeyMeta(KeyMetaType.digit, '7', CalcActions.PRESS_NUMBER),
      KeyMeta(KeyMetaType.digit, '8', CalcActions.PRESS_NUMBER),
      KeyMeta(KeyMetaType.digit, '9', CalcActions.PRESS_NUMBER),
      KeyMeta(KeyMetaType.operation, 'x', CalcActions.MULTIPLY),
    ],
    [
      KeyMeta(KeyMetaType.digit, '4', CalcActions.PRESS_NUMBER),
      KeyMeta(KeyMetaType.digit, '5', CalcActions.PRESS_NUMBER),
      KeyMeta(KeyMetaType.digit, '6', CalcActions.PRESS_NUMBER),
      KeyMeta(KeyMetaType.operation, '-', CalcActions.MINUS),
    ],
    [
      KeyMeta(KeyMetaType.digit, '1', CalcActions.PRESS_NUMBER),
      KeyMeta(KeyMetaType.digit, '2', CalcActions.PRESS_NUMBER),
      KeyMeta(KeyMetaType.digit, '3', CalcActions.PRESS_NUMBER),
      KeyMeta(KeyMetaType.operation, '+', CalcActions.PLUS),
    ],
    [
      KeyMeta(KeyMetaType.transform, '+-', CalcActions.CHANGE_SIGN),
      KeyMeta(KeyMetaType.digit, '0', CalcActions.PRESS_NUMBER),
      KeyMeta(KeyMetaType.transform, ',', CalcActions.COMMA),
      KeyMeta(KeyMetaType.system, '=', CalcActions.RESULT),
    ],
  ];

  @override
  Widget build(BuildContext context) {
    List<Widget> keyboard = [];

    for (var element in _keys) {
      List<Widget> buttonLine = [];
      for (var el in element) {
        Widget drawPromise = el.type == KeyMetaType.operation || el.type == KeyMetaType.unary_operation
            ? Image.asset('assets/icons/${el.label}.png')
            : Text(
                el.label,
                style: const TextStyle(
                  color: Colors.white,
                  fontSize: 25,
                ),
              );
        final buttonColor;
        if (el.type == KeyMetaType.operation || el.type == KeyMetaType.unary_operation ) {
          buttonColor = const Color(0xFF313334);
        } else if (el.type == KeyMetaType.digit) {
          buttonColor = const Color(0xFF373D3E);
        } else if (el.type == KeyMetaType.system) {
          buttonColor = const Color(0xFFE76608);
        } else {
          buttonColor = const Color(0xFF23282B);
        }
        buttonLine.add(Padding(
          padding: const EdgeInsets.all(2),
          child: InkWell(
            hoverColor: Colors.red,
            child: Ink(
              //  alignment: Alignment.center,
              width: MediaQuery.of(context).size.width / 4 - 7,
              decoration: BoxDecoration(
                color: buttonColor,
                borderRadius: BorderRadius.all(Radius.circular(5)),
              ),
              height: 55,
              // color: buttonColor,
              child: Center(
                  child:
                      drawPromise), //Image.asset('assets/icons/${el.label}.png'),
            ),
            onTap: () {
              _service.process('', el);
              print(el.label);
            },
          ),
        ));
      }
      keyboard.add(Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: buttonLine,
      ));
    }
    return Column(
      children: keyboard,
    );
  }
}
