import 'package:flutter/material.dart';

enum CalcColors {
  DIGITS(colorValue: 0xFF37423F),
  SYSTEM(colorValue: 0xFF1C2123),
  OPERATION(colorValue: 0xFF313334),
  HISTORY(colorValue: 0xFF23282B),
  ACTIVE_CURSOR(colorValue: 0xFFE76608),
  RESULT_BUTTON(colorValue: 0xFFE76608),
  TEXT(colorValue: 0xFFFFFFFF);

  const CalcColors({
    required this.colorValue,
  });

  final int colorValue;

  Color get () => Color(colorValue);

}
