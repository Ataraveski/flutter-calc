import 'package:flutter/material.dart';

import 'calcActions.dart';

class KeyMeta {

  KeyMetaType type;
  String label;
  CalcActions action;

  KeyMeta(this.type, this.label, this.action);
}

enum KeyMetaType {
  digit,
  operation,
  unary_operation,
  transform,
  system
}